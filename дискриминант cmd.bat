@echo off
@title calc
:path
echo.
echo "discriminant"
echo "the equation has the form ax^2 + bx + c"
set /p var="a:"
cls
echo "The equation has the form %var% x^2 + bx + c"
set /p var1="b:"
cls
echo "The equation has the form %var% x^2 + %var1% x + c"
set /p var2="c:"
cls
echo "The equation has the form %var% x^2 + %var1% x + (%var2%)"
set /a var3=(%var1%*%var1%)-(4*%var2%*%var%)
echo Discriminant = %var3%

pause>nul
cls
goto path